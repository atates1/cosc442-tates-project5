import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>JamesBondTest</code> contains tests for the class <code>{@link JamesBond}</code>.
 *
 * @generatedBy CodePro at 4/26/20, 12:24 PM
 * @author aniesetates
 * @version $Revision: 1.0 $
 */
public class JamesBondTest {
	/**
	 * Run the boolean bondRegex(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 4/26/20, 12:24 PM
	 */
	@Test
	public void testBondRegex_1()
		throws Exception {
		String regex = "";

		boolean result = JamesBond.bondRegex(regex);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NoSuchMethodException: bondRegex
		//       at com.instantiations.eclipse.analysis.expression.model.MethodInvocationExpression.findMethod(MethodInvocationExpression.java:711)
		//       at com.instantiations.eclipse.analysis.expression.model.MethodInvocationExpression.execute(MethodInvocationExpression.java:571)
		//       at com.instantiations.assist.eclipse.junit.execution.core.ExecutionRequest.execute(ExecutionRequest.java:286)
		//       at com.instantiations.assist.eclipse.junit.execution.communication.LocalExecutionClient$1.run(LocalExecutionClient.java:158)
		//       at java.base/java.lang.Thread.run(Thread.java:830)
		assertTrue(result);
	}

	@Test
	public void testCase0() {
	assertFalse(JamesBond.bondRegex("("));
	}

	@Test
	public void testCase1() {
	assertFalse(JamesBond.bondRegex("(("));
	}

	@Test
	public void testCase2() {
	assertFalse(JamesBond.bondRegex("((("));
	}

	@Test
	public void testCase3() {
	assertFalse(JamesBond.bondRegex("(()"));
	}

	@Test
	public void testCase4() {
	assertFalse(JamesBond.bondRegex("((007)"));
	}

	@Test
	public void testCase5() {
	assertFalse(JamesBond.bondRegex("((07)"));
	}

	@Test
	public void testCase6() {
	assertFalse(JamesBond.bondRegex("((7)"));
	}

	@Test
	public void testCase7() {
	assertFalse(JamesBond.bondRegex("()"));
	}

	@Test
	public void testCase8() {
	assertFalse(JamesBond.bondRegex("()("));
	}

	@Test
	public void testCase9() {
	assertFalse(JamesBond.bondRegex("())"));
	}

	@Test
	public void testCase10() {
	assertFalse(JamesBond.bondRegex("()007)"));
	}

	@Test
	public void testCase11() {
	assertFalse(JamesBond.bondRegex("()07)"));
	}

	@Test
	public void testCase12() {
	assertFalse(JamesBond.bondRegex("()7)"));
	}

	@Test
	public void testCase13() {
	assertFalse(JamesBond.bondRegex("(0("));
	}

	@Test
	public void testCase14() {
	assertFalse(JamesBond.bondRegex("(0(("));
	}

	@Test
	public void testCase15() {
	assertFalse(JamesBond.bondRegex("(0()"));
	}

	@Test
	public void testCase16() {
	assertFalse(JamesBond.bondRegex("(0(007)"));
	}

	@Test
	public void testCase17() {
	assertFalse(JamesBond.bondRegex("(0(07)"));
	}

	@Test
	public void testCase18() {
	assertFalse(JamesBond.bondRegex("(0(7)"));
	}

	@Test
	public void testCase19() {
	assertFalse(JamesBond.bondRegex("(0)"));
	}

	@Test
	public void testCase20() {
	assertFalse(JamesBond.bondRegex("(0)("));
	}
}